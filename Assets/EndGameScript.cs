﻿using UnityEngine;
using System.Collections;

public class EndGameScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void Awake()
    {
        Invoke("EndGame", 5f);

        iTween.CameraFadeAdd();
        iTween.CameraFadeDepth(0);
        iTween.CameraFadeTo(iTween.Hash("amount", 1, "time", 2));
//        iTween.CameraFadeTo(iTween.Hash("amount", 1, "time", 1, "oncomplete", "ReloadScene", "oncompletetarget", gameObject)); 
//        iTween.CameraFadeTo(0, .5f);
    }

    protected void EndGame()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
//        Application.Quit();
    }        


	// Update is called once per frame
	void Update () {
	
	}
}
