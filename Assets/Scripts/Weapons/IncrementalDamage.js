﻿#pragma strict
public var meshToUseAt5 : Mesh;
public var meshToUseAt4 : Mesh;
public var meshToUseAt3 : Mesh;
public var meshToUseAt2 : Mesh;
public var meshToUseAt1 : Mesh;

private var healthLastChecked : int;

function Start () {
     healthLastChecked = this.GetComponent(Health).health;
}

function Update () {
}

function IncrementDamage () {
     var currentHealth : int = GetComponent(Health).health;
     var maxHealth : int = GetComponent(Health).maxHealth;
     
     if (meshToUseAt1 != null && 100*currentHealth/maxHealth <= 17 && 100*healthLastChecked/maxHealth > 17) {
     	// We've transitioned below 1 (17%).  Show the new mesh.
     	this.GetComponent(MeshFilter).mesh = meshToUseAt1;
     }
     else if (meshToUseAt2 != null && 100*currentHealth/maxHealth <= 33 && 100*healthLastChecked/maxHealth > 33) {
     	// We've transitioned below 2 (33%).  Show the new mesh.
     	this.GetComponent(MeshFilter).mesh = meshToUseAt2;
     }
     else if (meshToUseAt3 != null && 100*currentHealth/maxHealth <= 50 && 100*healthLastChecked/maxHealth > 50) {
     	// We've transitioned below 3 (50%).  Show the new mesh.
     	this.GetComponent(MeshFilter).mesh = meshToUseAt3;
     }
     else if (meshToUseAt4 != null && 100*currentHealth/maxHealth <= 67 && 100*healthLastChecked/maxHealth > 67) {
     	// We've transitioned below 4 (67%).  Show the new mesh.
     	this.GetComponent(MeshFilter).mesh = meshToUseAt4;
     }
     else if (meshToUseAt5 != null && 100*currentHealth/maxHealth <= 83 && 100*healthLastChecked/maxHealth > 83) {
     	// We've transitioned below 5 (83%).  Show the new mesh.
     	this.GetComponent(MeshFilter).mesh = meshToUseAt5;
     }
     
     // We're all done, set our current health to be the latest for the next time we check.
     healthLastChecked = currentHealth;
}