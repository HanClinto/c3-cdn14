﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class C3
{
    /// <summary>
    /// An enumerated list of all layers in the game.  
    /// NOTE: This must be kept up to date MANUALLY with the list found in Edit -> Project Settings -> Tags and Layers
    /// Alternatively, if we don't want to use this, we can use LayerMask.LayerByName to retrieve the int value for a specified layer (usually done once during Awake and then cached for later reference)
    /// </summary>
    public enum Layers
    {
        Default         = 0,
        TransparentFX   = 1,
        IgnoreRaycast   = 2,
        Unused3         = 3,
        Water           = 4,
        UI              = 5,
        Unused6         = 6,
        Unused7         = 7,
        Player          = 8,
        Enemies         = 9,
        Environment     = 10,
        Ground          = 11,
        Unused12        = 12,
        Unused13        = 13,
        Unused14        = 14,
        Unused15        = 15,
        Unused16        = 16,
        Unused17        = 17,
        Unused18        = 18,
        Unused19        = 19,
        DamageSender    = 20,
    }
}
