﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class C3Cutscene : MonoBehaviour
{
    public GUITexture LPortrait;
    public GUIText LText;
    public GUITexture RPortrait;
    public GUIText RText;
    public GUITexture BG;

    public AudioSource DuringDialogAudio;
    public AudioSource AfterDialogAudio;

    [System.Serializable]
    public class Actor
    {
        public string Name;
        public Texture Texture;
        public bool OnRight;
    }

    public class ActorLine
    {
        public Actor Speaker;
        public string Text;
        public float Progress;
    }

    public Actor[] Actors;
    public String LinesBlob;

    private List<ActorLine> Lines = new List<ActorLine>();
    private int currentLine = 0;

    Actor lookupActor(string name)
    {
        foreach (var Char in Actors)
        {
            if (Char.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                return Char;
        }

        throw new Exception(String.Format("DIALOG ERROR!  Unable to find cutscene character name for '{0}' within list.", name));
        return null;
    }

	// Use this for initialization
	void Start () {
	    // Parse the LinesBlob into Lines:
	    var LinesBlobSplit = LinesBlob.Split('\n');

	    ActorLine currentLine = null;

//        Debug.Log("Setting up dialog!");

	    foreach (string lineBlob in LinesBlobSplit)
	    {
	        var parts = lineBlob.Split('\t');

	        if (parts.Length == 2)
	        {
	            var speakerName = parts[0].Trim(": ".ToCharArray());
	            var line = parts[1].Trim();

	            if (String.IsNullOrEmpty(speakerName))
	            {
	                if (!String.IsNullOrEmpty(line))
	                {
	                    // If there is no speaker, then append this text to the previous line.
	                    if (currentLine != null)
	                        currentLine.Text += "\n" + line;
	                }
	            }
	            else
	            {
                    currentLine = new ActorLine();
	                currentLine.Speaker = lookupActor(speakerName);
                    currentLine.Text = line;

                    Lines.Add(currentLine);
	            }
	        }
            else
	        {
	            throw new Exception(string.Format("ERROR!  Text '{0}' must have exactly one tab in it, separating actor name from the line.  Ex: 'Hero:\tWhat a mess!'.  Make sure when you populate the lineBlob that you select two (and exactly two) columns.", lineBlob));
	        }
	    }
        /*
	    foreach (var l in Lines)
	    {
            Debug.Log(String.Format("{0}: '{1}'", l.Speaker.Name, l.Text));
	    }
         * */
    
        this.currentLine = 0;
        SetupLine();
    }

    void Awake()
    {
        // Pause the game
        Time.timeScale = 0;

        // Play any sound to happen during the cutscene

        if (DuringDialogAudio != null)
            DuringDialogAudio.Play();
    }

    void SetupLine()
    {
        LPortrait.enabled = false;
        LText.enabled = false;
        RPortrait.enabled = false;
        RText.enabled = false;
        BG.enabled = false;

        if (Time.timeScale >= 1)
            return;

        if (currentLine >= Lines.Count)
        {
//            Debug.Log("No more lines!  Unpausing...");
            Time.timeScale = 1;

            if (DuringDialogAudio != null)
                DuringDialogAudio.Stop();
            
            if (AfterDialogAudio != null)
                AfterDialogAudio.Play();
        }
        else
        {
//            Debug.Log("Dialog is progressing!");

            // Ensure that the game is paused
            Time.timeScale = 0;

            var line = Lines[currentLine];

            BG.enabled = true;

            RPortrait.texture =
                LPortrait.texture =
                    line.Speaker.Texture;

            RText.text =
                LText.text =
                    line.Text;

            RPortrait.enabled = RText.enabled = line.Speaker.OnRight;
            LPortrait.enabled = LText.enabled = !line.Speaker.OnRight;
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (Time.timeScale >= 1)
	        return;

        ActorLine current;

        if (currentLine >= Lines.Count)
            return;
	    current = Lines[currentLine];

        // Advance

        // If we're pressing escape, skip to the end.
	    if (Input.GetKey(KeyCode.Escape))
	    {
            currentLine = Lines.Count;
            SetupLine();
	        return;
	    }

        // If we're holding down spacebar / mouse button
        if (Input.GetMouseButtonDown(0) ||
            Input.anyKeyDown)
        {
            // If we're ready to advance...
            if (current.Progress >= current.Text.Length)
            {
                currentLine++;
                SetupLine();

                if (currentLine >= Lines.Count)
                    return;
                current = Lines[currentLine];
            }
            else
            {
                current.Progress = current.Text.Length;
            }
        }

        // Advance just normally via the clock...
        current.Progress += Time.unscaledDeltaTime * 40f;

        // Update the text with our current values
	    RText.text = LText.text =
	        current.Text.Substring(0,
	            Mathf.Min((int) current.Progress, current.Text.Length));
	}
}
