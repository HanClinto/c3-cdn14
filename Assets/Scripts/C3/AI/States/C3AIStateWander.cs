using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class C3AIStateWander : C3AIState
{
	public float wanderSpeed = 5.0f;
	public float wanderAcceleration = 5.0f;
	public int minWanderDistance = 4;
	public int maxWanderDistance = 8;
	public int wanderArcWidth = 90;
	public Transform wanderArcTarget;
	public float wanderTargetThreshold = 1.0f;
	public string wanderTargetTag;
	public string wanderTargetName;
	public float wanderTimeInSeconds = 4.0f;

	private float _timeDoneState;

    public void OnEnable()
    {
		if (FirstRun) { FirstRun = false; return; }

		_timeDoneState = Time.time + wanderTimeInSeconds;

		if (!String.IsNullOrEmpty(wanderTargetTag))
		{
			float bestTarget = float.MaxValue;
			GameObject[] enemies = GameObject.FindGameObjectsWithTag(wanderTargetTag);
			foreach (GameObject enemy in enemies)
			{
				if (enemy.name == wanderTargetName || String.IsNullOrEmpty(wanderTargetName))
				{
					float currentTarget = Vector3.Distance(enemy.transform.position, Self.transform.position);
					if (currentTarget < bestTarget)
					{
						bestTarget = currentTarget;
						wanderArcTarget = enemy.transform;
					}
				}
			}
			if (bestTarget == float.MaxValue) { wanderArcTarget = null; }
		}

		Vector3 wanderArcCenter = Vector3.zero;
		if (wanderArcTarget == null)
		{ 
			wanderArcCenter = Self.transform.forward; 
		}
		else
		{ 
			wanderArcCenter = wanderArcTarget.position - Self.transform.position; 
		}
		// Start with the current angle.
		float targetAngle = Vector2.Angle(Vector2.right, new Vector2(wanderArcCenter.x, wanderArcCenter.z)) * Mathf.Sign(wanderArcCenter.z);
		// Add a random angle modifier based on the wanderArcWidth
		targetAngle += UnityEngine.Random.Range(0, wanderArcWidth) - (wanderArcWidth / 2.0f);
		Vector3 randomTarget = new Vector3(Mathf.Cos(targetAngle * Mathf.Deg2Rad), 0.0f, Mathf.Sin(targetAngle * Mathf.Deg2Rad));
		randomTarget *= UnityEngine.Random.Range (minWanderDistance, maxWanderDistance);

		Motor.movementTarget = Self.transform.localPosition + randomTarget;
		Motor.facingDirection = Motor.movementTarget - Self.transform.position;
		Motor.facingDirection.Normalize();
		Motor.movementDirection = Motor.facingDirection;

//		Debug.Log(String.Format("Enabling Wander Towards [{0}]...", Motor.movementTarget));
	}

    private int frameCnt = 0;

    public void Update()
    {
		Motor.movementSpeed = wanderSpeed;
		Motor.movementAcceleration = wanderAcceleration;

        var pos = Self.transform.localPosition;

		var distTarget = float.MaxValue;
		if (wanderArcTarget != null) { distTarget = Vector3.Distance(pos, wanderArcTarget.position); }
		var distWander = Vector3.Distance(pos, Motor.movementTarget);

		if (distTarget < wanderTargetThreshold)
		{
			PostMessage("Target_Reached");
		}
		else if (Time.time > _timeDoneState || distWander < wanderTargetThreshold)
		{
			PostMessage("Done");
		}
	}
}
