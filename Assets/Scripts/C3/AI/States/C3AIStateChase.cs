﻿using UnityEngine;
using System;
using System.Collections;

public class C3AIStateChase : C3AIState
{
	public float chaseSpeed = 10.0f;

	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }
		
		Motor.movementSpeed = chaseSpeed;
		Motor.movementAcceleration = 1.0f;
		
		//Debug.Log(String.Format("Enabling Chase Towards [{0}]...", Target.transform.position));
	}
	
	public void Update()
	{
		var m = Motor;
		
		m.movementTarget = Target.transform.position;
		
		m.facingDirection = (Target.transform.position - Self.transform.position);
		m.facingDirection.Normalize();
		
		m.movementDirection = m.facingDirection;
	}
}

