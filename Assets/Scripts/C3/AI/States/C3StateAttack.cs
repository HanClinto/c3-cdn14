using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class C3AIStateAttack : C3AIState
{
	public float attackDistance = 2.0f;
	public float attackTimeInSeconds = 3.0f;
	public float attackSpeed = 5.0f;
	public float attackAcceleration = 5.0f;
	public GameObject spawnObject = null;
	public int spawnObjectCount = 0;
	
	private float _timeDoneState;
	private int _spawnedObjectsCount;
	
	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }
		
		_timeDoneState = Time.time + attackTimeInSeconds;
		_spawnedObjectsCount = 0;

		Motor.movementSpeed = 0.0f;
		Motor.movementAcceleration = 0.0f;
		Self.rigidbody.velocity = Vector3.zero;
		//Motor.movementTarget = Target.transform.position;
		Motor.facingDirection = Target.transform.position - Self.transform.position;
		Quaternion rotation = new Quaternion();
		rotation.SetLookRotation(new Vector3(Target.transform.position.x - Self.transform.position.x, 0, Target.transform.position.z - Self.transform.position.z), Self.transform.up);
		Self.transform.rotation = rotation;

		//Debug.Log(String.Format("Enabling Attack..."));
	}
	
	private int frameCnt = 0;
	
	public void Update()
	{

		// TODO: Attack animation
		if (spawnObject != null)
		{
			int spawnTickCount = Mathf.RoundToInt((Time.time - 0.3f - (_timeDoneState - attackTimeInSeconds)) / (attackTimeInSeconds / spawnObjectCount));
			if (_spawnedObjectsCount < spawnTickCount)
			{
				for (int i = 1; i <= (spawnTickCount - _spawnedObjectsCount); i++)
				{
					var naggingEnemy = Instantiate(spawnObject, Self.transform.position, Self.transform.rotation) as GameObject;
					naggingEnemy.rigidbody.AddForce(Self.transform.forward * 1000.0f);
				}
				_spawnedObjectsCount = spawnTickCount;
			}
		}
		// TODO: Deal damage

		if (Time.time > _timeDoneState)
		{
			PostMessage("Done");
		}
	}
	
}
