﻿using UnityEngine;
using System;
using System.Collections;

public class C3AIStateIdle : C3AIState
{
	public float idleTimeInSeconds = 2;

	private float _timeDoneState;

	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }

		_timeDoneState = Time.time + idleTimeInSeconds;

		Motor.movementDirection = Vector3.zero;
		Motor.movementSpeed = 0.0f;
		Motor.movementAcceleration = 50.0f;
		Motor.movementTarget = Self.transform.position;
	}
	
	public void Update()
	{
		if (Time.time > _timeDoneState)
		{
			PostMessage("Done");
		}
	}
}
