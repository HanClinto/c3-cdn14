using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIState : MonoBehaviour
{
    // A flag that indicates whether or not we've run yet, in order to work around the bug that Unity will always call OnEnable for newly added components -- even when they're disabled initially.
    protected bool FirstRun = true;

    public void PostMessage(string message, GameObject context = null)
    {

        if (OnMessage != null)
        {
            var e = new C3AIEventArgs(this, message, context);
            OnMessage(this, e);
        }
    }

    protected void LookAtSlerp(Vector3 targetPosition, float damping = 2)
    {
        var lookPos = targetPosition - Self.transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        Self.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping); 
    }

    public event EventHandler<C3AIEventArgs> OnMessage;

    public C3AIManager Manager
    {
        get
        {
            return gameObject.GetComponent<C3AIManager>();
        }
    }

    public GameObject Self
    {
        get { return Manager.Self; }
    }

    public GameObject Target
    {
        get { return Manager.Target; }
    }

    public MovementMotor Motor
    {
        get
        {
            var mot = Manager.Self.GetComponent<MovementMotor>();

            if (mot == null)
                Debug.Log(string.Format("Unable to locate MovementMotor on object {0}", Self));

            return mot;
        }
    }

    public Animator Anim
    {
        get { return Manager.Anim; }
    }
}


public class C3AIEventArgs : EventArgs
{
    public C3AIState State;
    public string Message;
    public GameObject Context;

    public C3AIEventArgs(C3AIState state, string message, GameObject context)
    {
        State = state;
        Message = message;
        Context = context;
    }
}