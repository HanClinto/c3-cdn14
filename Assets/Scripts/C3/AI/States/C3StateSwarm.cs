﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class C3AIStateSwarm : C3AIState
{
	public float swarmTimeInSeconds = 5.0f;
	public float swarmSpeed = 5.0f;
	public float swarmAcceleration = 5.0f;

	private float _timeDoneState; 
	
	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }
		
		_timeDoneState = Time.time + swarmTimeInSeconds;

		Vector3 naggingLeadTarget = Self.transform.position;
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		foreach (GameObject enemy in enemies)
		{
			if (enemy.name == "Bitterness") { naggingLeadTarget = enemy.transform.position; break; }
			if (enemy.name == "Nagging") { naggingLeadTarget = enemy.transform.position; }
		}

		if (naggingLeadTarget == Self.transform.position)
		{
			Vector2 randomBreakAway = UnityEngine.Random.insideUnitCircle.normalized * 10;
			naggingLeadTarget = new Vector3(randomBreakAway.x, 0, randomBreakAway.y);
		}
		else
		{
			naggingLeadTarget.y = 0; // Keep them from drifting up or down over time.
			Motor.movementTarget = naggingLeadTarget + UnityEngine.Random.onUnitSphere;
		}

		//Debug.Log(String.Format("Enabling Swarm..."));
	}
	
	private int frameCnt = 0;
	
	public void Update()
	{
		//UnityEngine.Random.

		Motor.movementSpeed = swarmSpeed;
		Motor.movementAcceleration = swarmAcceleration;
		Motor.facingDirection = Motor.movementTarget - Self.transform.position;
		Motor.facingDirection.Normalize();
		Motor.movementDirection = Motor.facingDirection;

		if (Time.time > _timeDoneState)
		{
			PostMessage("Done");
		}
	}

}
