﻿using UnityEngine;
using System;
using System.Collections;

public class C3AIStateCharge : C3AIState
{
	public float chargeTimeInSeconds = 2.0f;
	public float chargeAcceleration = 1.0f;
	public float chargeSpeed = 20.0f;

	private float _timeDoneState; 

	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }

		_timeDoneState = Time.time + chargeTimeInSeconds;
		
		Motor.movementSpeed = chargeSpeed;
		Motor.movementAcceleration = chargeAcceleration;
		//Motor.movementTarget = Target.transform.position;
		Motor.facingDirection = (Target.transform.position - Self.transform.position).normalized;
		Motor.movementDirection = Motor.facingDirection;

		//Debug.Log(String.Format("Enabling Charge Towards [{0}]...", Target.transform.position));
	}

	public void Update()
	{
		if (Time.time > _timeDoneState)
		{
			PostMessage ("Done");
		}
	}
}

