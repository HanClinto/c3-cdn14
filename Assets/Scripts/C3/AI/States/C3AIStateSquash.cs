﻿using UnityEngine;
using System;
using System.Collections;

public class C3AIStateSquash : C3AIState
{
	protected Vector3 TakeoffLocation;
	protected Vector3 LandingLocation;
	
	public float JumpDuration = .5f;
	public float JumpHeight = 2;
	
	protected float TakeoffTime;
	
	public void OnEnable()
	{
		if (FirstRun) { FirstRun = false; return; }

//		Debug.Log(String.Format("Enabling Squash Towards [{0}]...", Target.transform.position));
		StartJump();
	}
	
	public void OnDisable()
	{
//		Debug.Log("Disabling Squash...");
		
		var m = Motor;
		m.enabled = true;
		
		// Reset the movement to be still
		m.movementTarget = Self.transform.position;
		m.facingDirection = Self.transform.forward;
		m.movementDirection = Vector3.zero;
	}
	
	protected void StartJump()
	{
		var m = Motor;
		m.enabled = false;
		
		TakeoffLocation = Self.transform.position;
		LandingLocation = Target.transform.position;
		TakeoffTime = Time.time;
		
		passedMark = false;
	}
	
	protected void FinishJump()
	{
		var m = Motor;
		m.enabled = true;
		
		Self.transform.position = LandingLocation;
		
		// TODO: Spawn damage radius
	}
	
	private bool passedMark = false;
	public float markPosition = 0.75f;
	private const float preJumpDelay = 1.0f;
	
	
	public void Update()
	{
		// Ensure that the motor is disabled
		//        var m = Motor;
		//        m.enabled = false;
		
		// Slowly turn towards the direction that we are going to face
		LookAtSlerp(LandingLocation);
		
		// Wait for the pre-jump-delay to happen
		if (Time.time - TakeoffTime > preJumpDelay)
		{
			float jumpTime = Time.time - TakeoffTime - preJumpDelay;
			float deltaT = (jumpTime)/JumpDuration;
			
			if (deltaT < 1)
			{
				// Move ourselves towards the destination in an arc
				Self.transform.position = JumpArcErp(TakeoffLocation, LandingLocation, JumpHeight, deltaT);
				
				// We need to trigger a "mark" when we're a certain way through the jump (but not quite done) so that we can start to trigger the landing animation.  Do that here.
				if (!passedMark && deltaT > markPosition) 
				{
					// We're now past our "mark", and can trigger the next animation.
					PostMessage("Ready_To_Attack");
					
					// Only fire this message once
					passedMark = true;
				}
			}
			else
			{
				FinishJump();
				PostMessage("Done");
			}
		}
	}
	
	/// <summary>
	/// Interpolates from one location to another with linear interpolation for X and Z, and adds a sine vertical "jump" arc on the Y axis.
	/// </summary>
	/// <returns></returns>
	public static Vector3 JumpArcErp(Vector3 start, Vector3 end, float yHeight, float value)
	{
		Vector3 retVal = Vector3.Lerp(start, end, value);
		
		float height = Mathf.Sin(value * Mathf.PI) * yHeight;
		
		retVal.y += height;
		
		return retVal;
	}
}
