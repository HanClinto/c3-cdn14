﻿using UnityEngine;
using System.Collections;

public class C3AIVision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log ("Starting up AI vision script!");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log (string.Format("Colliding {0} with this {1}", other.gameObject.tag, this.gameObject.tag));
	}
}
