using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIManager : MonoBehaviour {
	public GameObject Self;
	public GameObject Target;
    public Animator Anim;
    public ParticleSystem DeathParticleEffect;
    public ParticleSystem DamageParticleEffect;
	public GameObject DamagePrefab;
	public float AttackDistance = 5.0f;
	private int _CurrentStateIndex = -1;
	private Vector3 _startScale = Vector3.zero;

    protected bool dying = false;

	public enum AIStates {
		Attack,
		Charge,
		Chase,
		Dying,
		Idle,
		MeleeAttack,
		Squash,
		Stun,
		Swarm,
		Wander,
		WanderAway,
		Length // This state is unused, but is an end-of-enumerator marker to specify how many items are in this enum  // Brilliant, simply brilliant.
	}
	//protected List<C3AIState> States = new List<C3AIState>();
	// Size needs to match the number of AIStates above.
	protected C3AIState[] States = new C3AIState[(int)AIStates.Length];
	
	protected C3AIState AddState(AIStates stateToAdd)
	{
		switch (stateToAdd)
		{
		case AIStates.Attack: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateAttack>();
			break;
		case AIStates.Charge: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateCharge>();
			break;
		case AIStates.Chase: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateChase>();
			break;
		case AIStates.Dying:
			States[(int) stateToAdd] = gameObject.AddComponent<C3AIStateIdle>();
			break;
		case AIStates.Idle: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateIdle>();
			break;
		case AIStates.MeleeAttack: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateAttack>();
			break;
		case AIStates.Squash: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateSquash>();
			break;
		case AIStates.Stun: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateIdle>();
			break;
		case AIStates.Swarm: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateSwarm>();
			break;
		case AIStates.Wander: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateWander>();
			break;
		case AIStates.WanderAway: 
			States[(int)stateToAdd] = gameObject.AddComponent<C3AIStateWander>();
			break;
		}
		States[(int)stateToAdd].enabled = false;

		//  Set the first state added as the default.
		if (_CurrentStateIndex == -1) { _CurrentStateIndex = (int)stateToAdd; }

		return States[(int)stateToAdd];
	}

	public int CurrentStateIndex {
		get { return _CurrentStateIndex; }
	    private set
	    {
            if (_CurrentStateIndex != value)
	        {
	            GotoState(value);
	        }
	    }
	}

    protected C3AIState CurrentState
    {
        get { if ( CurrentStateIndex >= 0 &&
                   CurrentStateIndex < States.Length)
                return States[CurrentStateIndex];
            return null;
        }
    }
	
	public Renderer TargetRenderer
	{
		get { return Self.GetComponentInChildren<Renderer>(); }
	}

    public virtual void Awake()
    {
		if (Target == null) { Target = GameObject.FindGameObjectWithTag("Player"); }
		if (Self == null) { Self = gameObject; }
        
        MovementMotor m = Self.GetComponent<MovementMotor>();
        _startScale = Self.transform.localScale;

        // Subscribe to our damage receiver
        var rs = Self.gameObject.GetComponents<C3DamageReceiver>();
        foreach (var r in rs)
        {
            r.BeforeDamage += OnBeforeDamage;
            r.AfterDamage += OnAfterDamage;
            r.Died += OnDied;
        }
    }
	
	protected bool _isCollidingWithAttack = false;
	protected bool _isCollidingWithPlayer = false;
	protected bool _isCollidingWithEnvironment = false;
	protected bool _isCollidingWithEnemy = false;
	
	void OnCollisionEnter(Collision collision) {
		//Debug.Log(string.Format("CONTACT!!! [{0}] with [{1}]", this, collision.gameObject.layer));
		switch (collision.gameObject.layer)
		{
			case (int)C3.Layers.Ground: break; // Ground layer
			case (int)C3.Layers.Environment: _isCollidingWithEnvironment = true; break; // Environment layer
			case (int)C3.Layers.Enemies: _isCollidingWithEnemy = true; break; // Enemies layer
			case (int)C3.Layers.DamageSender: _isCollidingWithAttack = true; break; // Weapon layer
			case (int)C3.Layers.Player: _isCollidingWithPlayer = true; break; // Player layer.  Ha ha, that sounds funny!
		}
	}

    protected virtual void OnBeforeDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
//        float atten = UnityEngine.Random.Range(0f, 1f);

//        Debug.Log(String.Format("Entity {0} is due to receive {1} damage.  Attenuating by {2}.", Self.name, e.Amount, atten));

//        e.DamageAttenuation = atten;
    }

    protected virtual void OnAfterDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
        Debug.Log(String.Format("Entity {0} has received {1} damage!", Self.name, e.Amount));

        if (DamageParticleEffect != null)
        {
            DamageParticleEffect.Play();
        }
    }

    protected virtual void OnDied(object sender, C3DamageReceiver.DamageEventArgs e)
    {
        Debug.Log(String.Format("Entity {0} has died!", Self.name));

        StartCoroutine(DeathRoutine());
    }

    private float timeOfDeath = 0;
    IEnumerator DeathRoutine()
    {
        dying = true;
        timeOfDeath = Time.time;

        GotoState((int)AIStates.Dying);

        if (DeathParticleEffect != null)
        {
            DeathParticleEffect.Play();
        }

        yield return new WaitForSeconds(6);

        Destroy(Self);
    }

    protected virtual void Update()
    {
		_isCollidingWithAttack = false;
		_isCollidingWithPlayer = false;
		_isCollidingWithEnvironment = false;
		_isCollidingWithEnemy = false;

        if (dying)
        {
            float t = (Time.time - timeOfDeath)*0.2f;
			Self.transform.localScale = Vector3.Lerp(_startScale, Vector3.zero, t);

            // If we've expired entirely, then 
            if (t >= 1)
            {
                Destroy(Self);
            }
        }
    }

    protected virtual void BuildAIStates()
    {
        // Override this in child classes
    }
    
    public virtual void Start () {
        // Start out in the first AI state (the default) 
        GotoState(_CurrentStateIndex);
	}

    /// <summary>
    /// Activates a new state, and deactivates the old state (if applicable)
    /// </summary>
    /// <param name="stateIndex">The new state to transition to</param>
    /// <param name="force">Whether or not to force transitioning to this state (as in an emergency)</param>
    protected void GotoState(int stateIndex, bool force = false)
    {
        // Disable the old state
        if (CurrentState != null)
        {
//            if (!CurrentState.CanExit && !force)
//			{
//				Debug.Log(string.Format("Can't move from state {0} from state {1}!", stateIndex, CurrentStateIndex));
//				return;
//			}

            CurrentState.enabled = false;
        }

        // Activate the new state
//		Debug.Log(
//			string.Format(
//				"Enemy [{0}][{1}] transitioning from state [{2}] to state [{3}].", 
//				Self.name,
//				Self.GetInstanceID(), 
//				Enum.GetName(typeof(AIStates), CurrentStateIndex),
//				Enum.GetName(typeof(AIStates), stateIndex) 
//			)
//  		);
		_CurrentStateIndex = stateIndex;

        CurrentState.enabled = true;
    }

	public float VisionArc = 90f;
	public float VisionDistance = 12.0f;

	private float firstSeenTime = 0;
	private bool alreadySeen = false;
	private const float seenThreshold = .1f; // How long the player must be visible before the enemy attacks
	
	protected bool playerSighted()
	{
		// If we haven't yet seen the character
		if (!alreadySeen)
		{
			// But if we can see him now
			if (CanSeeTarget(Target, VisionArc, VisionDistance))
			{
				// Then mark that we've seen him
				alreadySeen = true;
				firstSeenTime = Time.time;
			}
		}
		
		// If we've already seen the player long enough, trigger the Player_Sighted message.
		if (alreadySeen) // If we've already seen the character
		{
			// If we can still see the character
			if (CanSeeTarget(Target, VisionArc, VisionDistance))
			{
				// Check to see if we've retained vision contact long enough
				if (Time.time - firstSeenTime > seenThreshold)
				{
					alreadySeen = false;
					return true;
				}
				// Else, we haven't lost sight of the player, but we're biding our time
			}
			else
			{
				// We lost sight before the timer ran out -- don't register the view
				alreadySeen = false;
			}
		}
		return false;
	}
	
	public bool CanSeeTarget (GameObject sightTarget, float viewingAngle = 180, float viewingRadius = float.PositiveInfinity) 
	{
		Vector3 targetDirection = (sightTarget.transform.position - Self.transform.position);
        RaycastHit hit;

	    Vector3 forward = Self.transform.forward;

	    float deltaTheta = Vector3.Angle(forward, targetDirection.normalized);
        /*
        Debug.Log(string.Format("AI is facing {0}.  Target direction is {1}.  Delta is {2}, viewing angle is {3}", 
                                forward, 
                                targetDirection.normalized, 
                                deltaTheta,
                                viewingAngle));
        */

        // Ensure we're within the viewing angle
        if (deltaTheta > viewingAngle)
	        return false;

        // Ensure we're within the viewing radius
	    if (targetDirection.magnitude > viewingRadius)
	        return false;

        Physics.Raycast (Self.transform.position, targetDirection, out hit, targetDirection.magnitude);
        
		if (hit.transform == sightTarget.transform)
			return true;

		return false;
	}
}

