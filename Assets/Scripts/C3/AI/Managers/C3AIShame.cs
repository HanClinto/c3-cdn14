﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIShame : C3AIManager
{
	public int MinWanderRange = 5;
	public int MaxWanderRange = 7;
	public int wanderArcWidth = 180;
	public float WanderSpeed = 10.0f;
	public float WanderAcceleration = 1.0f;
	public float JumpDuration = .5f;
	public float JumpHeight = 2;
	public float StunTimeInSeconds = 3.0f;

    public override void Awake()
    {
        base.Awake();

		var wander = (C3AIStateWander)AddState(AIStates.Wander);
		var idle = (C3AIStateIdle)AddState(AIStates.Idle);
		var chase = (C3AIStateChase)AddState(AIStates.Chase);
		var squash = (C3AIStateSquash)AddState(AIStates.Squash);
		var stun = (C3AIStateIdle)AddState(AIStates.Stun);
        var dying = (C3AIStateIdle) AddState(AIStates.Dying);

        // Configure the Idle state
        idle.idleTimeInSeconds = 3.0f;
        idle.OnMessage += (sender, args) =>
        {
            switch (args.Message)
            {
				case "Player_Sighted":
				case "Collided_With_Attack":
				case "Collided_With_Player":
					TargetRenderer.material.color = Color.magenta;
					GotoState((int)AIStates.Chase);
					break;
				case "Done":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
			}
        };

        // Configure the Wander state
		wander.minWanderDistance = MinWanderRange;
		wander.maxWanderDistance = MaxWanderRange;
		wander.wanderArcWidth = wanderArcWidth;
		wander.wanderArcTarget = Target.transform;
		wander.wanderSpeed = WanderSpeed;
		wander.wanderAcceleration = WanderAcceleration;
		wander.OnMessage += (sender, args) =>
        {
            switch (args.Message)
            {
				case "Player_Sighted":
				case "Collided_With_Attack":
				case "Collided_With_Player":
					TargetRenderer.material.color = Color.magenta;
					GotoState((int)AIStates.Chase);
					break;
				case "Collided_With_Environment":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
				case "Done":
				case "Target_Reached":
					TargetRenderer.material.color = Color.green;
					GotoState((int) AIStates.Idle);
                    break;
            }
        };

        // Configure the Chase state
		chase.chaseSpeed = WanderSpeed;
		chase.OnMessage += (sender, args) =>
        {
            switch (args.Message)
            {
				case "Collided_With_Attack":
				case "Collided_With_Environment":
				case "Player_In_Attack_Range":
					Anim.SetTrigger("Jump");
					TargetRenderer.material.color = Color.red;
					GotoState((int)AIStates.Squash);
					break;
				case "Done":
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Idle);
					break;
			}
        };

        // Configure the Squash state
		squash.JumpDuration = JumpDuration;
		squash.JumpHeight = JumpHeight;
		squash.OnMessage += (sender, args) =>
        {
            switch (args.Message)
            {
                case "Ready_To_Attack":
                    // When the jump sequence has reached the specified point, trigger the bottom of the slam animation.
					TargetRenderer.material.color = Color.red;
                    Anim.SetTrigger("Slam");
                    break;
                case "Done":
                    // Switch to the "stun" stage, which plays the final squash animation
                    // Spawn an explosion prefab to damage and leave scorch marks
                    Invoke("SpawnExplosion", .25f);
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Stun);
                    break;
            }
        };

        // Configure the Stun state that takes over after Squash.
        
        // Set the idle time of this to exactly match the post-jump animation
		stun.idleTimeInSeconds = StunTimeInSeconds;
        stun.OnMessage += (sender, args) =>
        {
            switch (args.Message)
            {
                case "Done":
                    Anim.SetTrigger("Done");
					TargetRenderer.material.color = Color.cyan;
                    GotoState((int)AIStates.Wander);
                    break;
            }
        };

    }

    public void SpawnExplosion()
    {
        GameObject scorch =
            (GameObject)
				Instantiate(DamagePrefab,
                     new Vector3(this.transform.position.x,
                                  UnityEngine.Random.Range(0.0001f, 0.2f),
                                  this.transform.position.z),
                     Quaternion.Euler(270, UnityEngine.Random.Range(0, 360), 0));

        // Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.
        var damageSender = scorch.GetComponentInChildren<C3DamageSender>();

        damageSender.SendingLayer = Self.gameObject.layer;

        float smash_size = 4;
        scorch.transform.localScale = new Vector3(smash_size, smash_size, smash_size);
    }

    // Update is used to check for standard events (player_sighted, collided_with_obstacle, etc.) so they can be kicked off as messages.
    public virtual void Update()
    {
		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();

		switch ( (AIStates)CurrentStateIndex )
		{
			case AIStates.Idle:
			case AIStates.Wander:
			{
				// Priority of event triggers:
				//  Collided_With_Attack
				//  Collided_With_Player
				//  In_Attack_Range
				//  Player_Sighted
				//  Collided_With_Environment
				//  Collided_With_Enemy

				if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
					break;
				}
				else if (isCollidingWithPlayer)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Player");
					break;
				}
				else if (playerSighted())
				{
					States[CurrentStateIndex].PostMessage("Player_Sighted");
					break;
				}
				else if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}
				break;
			}
			// Only check to attack if we are currently chasing...
			case AIStates.Chase:
			{
				if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
					break;
				}
				else if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}				// We're in range to attack...
				else if ((Target.transform.position - Self.transform.position).magnitude < AttackDistance)
				{
					// Then jump towards the target
					States[CurrentStateIndex].PostMessage("Player_In_Attack_Range");
				}
				break;
			}
		}

        Vector3 speed = Self.rigidbody.velocity;
        float speedVert = speed.y;
        speed.y = 0;
        float speedHoriz = speed.magnitude;
        Anim.SetFloat("SpeedHoriz", speedHoriz);
    }

}
