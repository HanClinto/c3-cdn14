﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AITotem : C3AIManager
{
	public float AttackTimeInSeconds = 3.0f;
	public GameObject AttackSpawnObject = null;
	public int AttackSpawnObjectCount = 0;
	public GameObject TriggerOnObjectHealth;
	public float TriggerOnHealth = 0.4f;
	public float TriggerResetOnHealth = 0.75f;

	private bool _watchingHealth = true;
	private C3DamageReceiver _damageReceiver;

	public override void Awake()
	{
		base.Awake();
		if (TriggerOnObjectHealth != null) { _damageReceiver = TriggerOnObjectHealth.GetComponent<C3DamageReceiver>(); }

		var idle = (C3AIStateIdle)AddState(AIStates.Idle);
		var attack = (C3AIStateAttack)AddState(AIStates.Attack);
		var dying = (C3AIStateIdle) AddState(AIStates.Dying);
		
		// Configure the Idle state
		idle.idleTimeInSeconds = 1000.0f;
		idle.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Reached_Health_Threshold":
					TargetRenderer.material.color = Color.red;
					GotoState((int)AIStates.Attack);
					break;
				case "Done":
					TargetRenderer.material.color = Color.white;
					GotoState((int)AIStates.Idle);
					break;
			}
		};

		attack.spawnObject = AttackSpawnObject;
		attack.attackTimeInSeconds = AttackTimeInSeconds;
		attack.spawnObjectCount = AttackSpawnObjectCount;
		attack.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					TargetRenderer.material.color = Color.white;
					GotoState((int)AIStates.Idle);
					break;
			}
		};
	}
	
	public void SpawnExplosion()
	{
		GameObject scorch =
			(GameObject)
				Instantiate(DamagePrefab,
		            new Vector3(this.transform.position.x,
		            UnityEngine.Random.Range(0.0001f, 0.2f),
		            this.transform.position.z),
		            Quaternion.Euler(270, UnityEngine.Random.Range(0, 360), 0));
		
		// Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.
		var damageSender = scorch.GetComponentInChildren<C3DamageSender>();
		
		damageSender.SendingLayer = Self.gameObject.layer;
		
		float smash_size = 4;
		scorch.transform.localScale = new Vector3(smash_size, smash_size, smash_size);
	}
	
	// Update is used to check for standard events (player_sighted, collided_with_obstacle, etc.) so they can be kicked off as messages.
	public virtual void Update()
	{
		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();
		
		switch ( (AIStates)CurrentStateIndex )
		{
			case AIStates.Idle:
				if (_watchingHealth)
				{
					if ((_damageReceiver != null) && (_damageReceiver.Health / _damageReceiver.MaxHealth < TriggerOnHealth))
					{
						_watchingHealth = false;
						States[CurrentStateIndex].PostMessage("Reached_Health_Threshold");
						break;
					}
					else if ((_damageReceiver != null) && (_damageReceiver.Health / _damageReceiver.MaxHealth > TriggerResetOnHealth))
					{
						_watchingHealth = true;
					}
				}
				break;
		}
	}
	
}
