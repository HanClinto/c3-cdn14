﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AINagging : C3AIManager
{
	public int MinWanderRange = 5;
	public int MaxWanderRange = 8;
	public int wanderArcWidth = 270;
	public float WanderSpeed = 5.0f;
	public float WanderAcceleration = 5.0f;
	public float WanderTimeInSeconds = 1.2f;
	public float ChargeSpeed = 20.0f;
	public float AttackTimeInSeconds = 3.0f;

	public override void Awake()
	{
		base.Awake();
		
		var swarm = (C3AIStateSwarm)AddState(AIStates.Swarm);
		var wander = (C3AIStateWander)AddState(AIStates.Wander);
		var charge = (C3AIStateCharge)AddState(AIStates.Charge);
		var attack = (C3AIStateAttack)AddState(AIStates.Attack);
		var dying = (C3AIStateIdle) AddState(AIStates.Dying);

		// Configure the Wander state
		swarm.swarmTimeInSeconds = 2.1f;
		swarm.swarmSpeed = WanderSpeed;
		swarm.swarmAcceleration = WanderAcceleration;
		swarm.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					TargetRenderer.material.color = Color.blue;
					GotoState((int) AIStates.Wander);
					break;
			}
		};

		wander.minWanderDistance = MinWanderRange;
		wander.maxWanderDistance = MaxWanderRange;
		wander.wanderArcWidth = wanderArcWidth;
		wander.wanderArcTarget = Target.transform;
		wander.wanderSpeed = WanderSpeed;
		wander.wanderAcceleration = WanderAcceleration;
		wander.wanderTimeInSeconds = WanderTimeInSeconds;
		wander.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Player_Sighted":
				case "Collided_With_Attack":
				case "Collided_With_Player":
					TargetRenderer.material.color = Color.magenta;
					GotoState((int) AIStates.Charge);
					break;
				case "Done":
				case "Target_Reached":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int) AIStates.Swarm);
					break;
			}
		};

		charge.chargeSpeed = ChargeSpeed;
		charge.chargeTimeInSeconds = 0.7f;
		charge.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Player_In_Attack_Range":
                    Anim.SetTrigger("Attack");
					TargetRenderer.material.color = Color.red;
					GotoState((int)AIStates.Attack);
					break;
				case "Collided_With_Environment":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Swarm);
					break;
				case "Done":
					TargetRenderer.material.color = Color.blue;
					GotoState((int)AIStates.Wander);
					break;
			}
		};

		attack.attackDistance = AttackDistance;
		attack.attackTimeInSeconds = AttackTimeInSeconds;
		attack.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
                    Anim.SetTrigger("Idle");
					GotoState((int)AIStates.Swarm);
					break;
			}
		};
	}

	// Update is called once per frame
	public virtual void Update()
	{
		
		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();
		
		switch ( (AIStates)CurrentStateIndex )
		{
			case AIStates.Wander:
			{
				// Priority of event triggers:
				//  Collided_With_Attack
				//  Collided_With_Player
				//  In_Attack_Range
				//  Player_Sighted
				//  Collided_With_Environment
				//  Collided_With_Enemy
				
				if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
					break;
				}
				else if (isCollidingWithPlayer)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Player");
					break;
				}
				else if (playerSighted())
				{
					States[CurrentStateIndex].PostMessage("Player_Sighted");
					break;
				}
				break;
			}
			case AIStates.Charge:
			{
				if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}				// We're in range to attack...
				else if ((Target.transform.position - Self.transform.position).magnitude < AttackDistance)
				{
					// Then jump towards the target
					States[CurrentStateIndex].PostMessage("Player_In_Attack_Range");
				}
				break;
			}
		}
	}
	
}