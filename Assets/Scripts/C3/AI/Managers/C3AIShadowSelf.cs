﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIShadowSelf : C3AIManager
{
	public int MinWanderRange = 6;
	public int MaxWanderRange = 8;
	public int wanderArcWidth = 180;
	public float WanderSpeed = 5.0f;
	public float WanderAcceleration = 10.0f;
	public float ChargeSpeed = 30.0f;
	public float ChargeAcceleration = 2.0f;
	public float ChargeTimeInSeconds = 2.0f;
	public float RechargeStartAtHealthPercentage = 0.4f;
    public float RechargeTimeInSeconds = 8.0f;
	public float RechargeDoneAtHealthPercentage = 0.8f;
	public float RechargeRate = 20.0f;
	public float StunTimeInSeconds = 1.0f;

    // Combat configuration
    public GameObject meleePrefab;
    public Transform meleeTransform;
    
    private C3DamageReceiver _damageReceiver;
    
    public override void Awake()
	{
		base.Awake();
		_damageReceiver = gameObject.GetComponent<C3DamageReceiver>();

		var idle = (C3AIStateIdle)AddState(AIStates.Idle);
		var charge = (C3AIStateCharge)AddState(AIStates.Charge);
		var stun = (C3AIStateIdle)AddState(AIStates.Stun);
		var wander = (C3AIStateWander)AddState(AIStates.Wander);
		var dying = (C3AIStateIdle) AddState(AIStates.Dying);

		// This is the idle while waiting for health regeneration.
		idle.idleTimeInSeconds = RechargeTimeInSeconds;
		idle.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
                case "Reached_Health_Threshold":
                case "Done":
			        StartCoroutine(DoMeleeAttack(meleeTransform));
					TargetRenderer.material.color = Color.red;
					GotoState((int)AIStates.Charge);
					break;
			}
		};
		
		charge.chargeSpeed = ChargeSpeed;
		charge.chargeAcceleration = ChargeAcceleration;
		charge.chargeTimeInSeconds = ChargeTimeInSeconds;
		charge.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Collided_With_Environment":
                case "Collided_With_Attack":
                case "Done":
					//Anim.SetTrigger("Stun");
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Stun);
					break;
            }
        };
        
		stun.idleTimeInSeconds = StunTimeInSeconds;
		stun.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
                case "Done":
			        StartCoroutine(DoMeleeAttack(meleeTransform));
                    TargetRenderer.material.color = Color.red;
                    GotoState((int)AIStates.Charge);
                    break;
                case "Reached_Health_Threshold":
                    Anim.SetTrigger("Wander");
                    TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
                    break;
            }
        };
        
		// Wander back to a totem for regeneration.
        wander.minWanderDistance = MinWanderRange;
        wander.maxWanderDistance = MaxWanderRange;
		wander.wanderArcWidth = wanderArcWidth;
		wander.wanderArcTarget = Target.transform;
		wander.wanderTargetThreshold = 3.0f;
		wander.wanderTargetTag = "Enemy";
		wander.wanderTargetName = "Totem";
		wander.wanderSpeed = WanderSpeed;
		wander.wanderAcceleration = WanderAcceleration;
		wander.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
                case "Collided_With_Environment":
                case "Done":
                    TargetRenderer.material.color = Color.cyan;
                    GotoState((int)AIStates.Wander);
                    break;
                case "Target_Reached":
                    TargetRenderer.material.color = Color.white;
                    GotoState((int)AIStates.Idle);
                    break;
            }
        };
    }
    
    public void SpawnExplosion()
	{
		GameObject scorch =
			(GameObject)
				Instantiate(DamagePrefab,
				            new Vector3(this.transform.position.x,
				            UnityEngine.Random.Range(0.0001f, 0.2f),
				            this.transform.position.z),
				            Quaternion.Euler(270, UnityEngine.Random.Range(0, 360), 0));
		
		// Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.
		var damageSender = scorch.GetComponentInChildren<C3DamageSender>();
		damageSender.SendingLayer = Self.gameObject.layer;
		
		float smash_size = 4;
		scorch.transform.localScale = new Vector3(smash_size, smash_size, smash_size);
	}


    protected IEnumerator DoMeleeAttack(Transform from)
    {
        //        Debug.Log("Melee attack!");
        Anim.SetTrigger("MeleeAttack");

        yield return new WaitForSeconds(0.25f);

        // Instantiate the attack
        // Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.

        var atk = Instantiate(meleePrefab,
                              from.position,
                              from.rotation) as GameObject;

        // Set the sender of the damage so that it will affect the correct other parties
        var dmg = atk.GetComponentInChildren<C3DamageSender>();
        dmg.SendingLayer = (int)C3.Layers.Enemies;
        dmg.DamageAmount = 10;

        float smash_size = 2;
        atk.transform.localScale = new Vector3(smash_size, smash_size, smash_size);

        yield return null;
    }
	
	// Update is called once per frame
	public virtual void Update()
	{
		Anim.SetFloat("Speed", rigidbody.velocity.magnitude);
        
		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();
		
		switch ( (AIStates)CurrentStateIndex )
		{
            case AIStates.Idle:
				// Regenerate health while at totem.
				_damageReceiver.Health = Math.Min(_damageReceiver.Health + (Time.deltaTime * RechargeRate), _damageReceiver.MaxHealth);
                //if ((_damageReceiver != null) && (_damageReceiver.Health / _damageReceiver.MaxHealth > RechargeDoneAtHealthPercentage))
                //{ // Break out when the enemy has regenrated enough health.
                //    States[CurrentStateIndex].PostMessage("Reached_Health_Threshold");
                //    break;
                //}
                break;
            case AIStates.Stun:
                if ((_damageReceiver != null) && (_damageReceiver.Health / _damageReceiver.MaxHealth < RechargeStartAtHealthPercentage))
				{ // Seek regeneration whenever this enemy's health is low enough.
                    States[CurrentStateIndex].PostMessage("Reached_Health_Threshold");
					break;
                }
				break;
            case AIStates.Wander:
            {
                if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}
				break;
			}
			case AIStates.Charge:
			{
				if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}
				else if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
	                break;
                }
				break;
            }
		}
    }
    
}