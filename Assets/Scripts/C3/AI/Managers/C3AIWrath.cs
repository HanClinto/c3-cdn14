﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIWrath : C3AIManager
{
	public int MinWanderRange = 6;
	public int MaxWanderRange = 8;
	public int wanderArcWidth = 180;
	public float WanderSpeed = 5.0f;
	public float WanderAcceleration = 10.0f;
	public float ChargeSpeed = 20.0f;
	public float ChargeAcceleration = 1.0f;
	public float ChargeTimeInSeconds = 2.0f;
	public float StunTimeInSeconds = 3.0f;

	public override void Awake()
	{
		base.Awake();
		
		var wander = (C3AIStateWander)AddState(AIStates.Wander);
		var charge = (C3AIStateCharge)AddState(AIStates.Charge);
		var stun = (C3AIStateIdle)AddState(AIStates.Stun);
		var dying = (C3AIStateIdle) AddState(AIStates.Dying);

		// Configure the Wander state
		wander.minWanderDistance = MinWanderRange;
		wander.maxWanderDistance = MaxWanderRange;
		wander.wanderArcWidth = wanderArcWidth;
		wander.wanderArcTarget = Target.transform;
		wander.wanderSpeed = WanderSpeed;
		wander.wanderAcceleration = WanderAcceleration;
		wander.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Player_Sighted":
				case "Collided_With_Attack":
				case "Collided_With_Player":
					TargetRenderer.material.color = Color.red;
                    Anim.SetTrigger("Charge");
					GotoState((int)AIStates.Charge);
					break;
				case "Done":
                case "Target_Reached":
                case "Collided_With_Environment":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
			}
		};

		// Configure the Charge state
		charge.chargeSpeed = ChargeSpeed;
		charge.chargeAcceleration = ChargeAcceleration;
		charge.chargeTimeInSeconds = ChargeTimeInSeconds;
		charge.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Collided_With_Attack":
                case "Collided_With_Environment":
				case "Collided_With_Player":
                case "Player_In_Attack_Range":
                    Anim.SetTrigger("Stun");
					TargetRenderer.material.color = Color.black;
					// Spawn an explosion prefab to damage and leave scorch marks
					Invoke("SpawnExplosion", .25f);
					GotoState((int)AIStates.Dying);
					break;
				case "Done":
                    Anim.SetTrigger("Stun");
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Stun);
					break;
			}
		};

		stun.idleTimeInSeconds = StunTimeInSeconds;
		stun.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					Anim.SetTrigger("Wander");
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
			}
		};
	}
	
	public void SpawnExplosion()
	{
		GameObject scorch =
			(GameObject)
				Instantiate(DamagePrefab,
				            new Vector3(this.transform.position.x,
				            UnityEngine.Random.Range(0.0001f, 0.2f),
				            this.transform.position.z),
				            Quaternion.Euler(270, UnityEngine.Random.Range(0, 360), 0));

        Destroy(gameObject);
        // Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.
//        var damageSender = scorch.GetComponentInChildren<C3DamageSender>();
//        damageSender.SendingLayer = Self.gameObject.layer;
        
//        float smash_size = 4;
//		scorch.transform.localScale = new Vector3(smash_size, smash_size, smash_size);
	}
	
	// Update is called once per frame
	public virtual void Update()
	{

		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();
		
		switch ( (AIStates)CurrentStateIndex )
		{
			case AIStates.Idle:
			case AIStates.Wander:
			{
				if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
					break;
				}
				else if (isCollidingWithPlayer)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Player");
					break;
				}
				else if (playerSighted())
				{
					States[CurrentStateIndex].PostMessage("Player_Sighted");
					break;
				}
				else if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}
				break;
			}
			case AIStates.Charge:
			{
				if (isCollidingWithAttack)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Attack");
					break;
				}
				else if (isCollidingWithPlayer)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Player");
					break;
				}
				else if (isCollidingWithEnvironment)
				{
					States[CurrentStateIndex].PostMessage("Collided_With_Environment");
					break;
				}
				else if ((Target.transform.position - Self.transform.position).magnitude < AttackDistance)
				{ // We're in range to attack...
					States[CurrentStateIndex].PostMessage("Player_In_Attack_Range");
				}
				break;
			}
		}
	}

}