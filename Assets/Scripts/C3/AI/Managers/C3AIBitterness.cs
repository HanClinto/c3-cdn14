﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3AIBitterness : C3AIManager
{
	public int MinWanderRange = 5;
	public int MaxWanderRange = 7;
	public int WanderArcWidth = 180;
	public float WanderSpeed = 5.0f;
	public float WanderAcceleration = 10.0f;
	public float AttackTimeInSeconds = 3.0f;
	public GameObject AttackSpawnObject = null;
	public int AttackSpawnObjectCount = 0;
	public float StunTimeInSeconds = 3.0f;

    // Combat configuration
    public GameObject meleePrefab;
    public Transform meleeTransform;

	public override void Awake()
	{
		base.Awake();
		
		var wander = (C3AIStateWander)AddState(AIStates.Wander);
		var wanderAway = (C3AIStateWander)AddState(AIStates.WanderAway);
		var attack = (C3AIStateAttack)AddState(AIStates.Attack);
		var meleeAttack = (C3AIStateAttack)AddState(AIStates.MeleeAttack);
		var stun = (C3AIStateIdle)AddState(AIStates.Stun);
		var dying = (C3AIStateIdle) AddState(AIStates.Dying);

        wander.minWanderDistance = MinWanderRange;
		wander.maxWanderDistance = MaxWanderRange;
		wander.wanderArcWidth = WanderArcWidth;
		wander.wanderSpeed = WanderSpeed;
		wander.wanderAcceleration = WanderAcceleration;
		wander.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Player_Sighted":
				case "Collided_With_Attack":
					Anim.SetTrigger("Nagging");
					TargetRenderer.material.color = Color.magenta;
					GotoState((int)AIStates.Attack);
					break;
				case "Collided_With_Player":
				case "Player_In_Attack_Range":
					Anim.SetTrigger("Attack");
					TargetRenderer.material.color = Color.red;
					GotoState((int)AIStates.MeleeAttack);
					break;
				case "Collided_With_Environment":
				case "Target_Reached":
				case "Done":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
			}
		};
		
		wanderAway.minWanderDistance = -7;
		wanderAway.maxWanderDistance = -10;
		wanderAway.wanderArcWidth = 90;
		wanderAway.wanderArcTarget = Target.transform;
		wanderAway.wanderSpeed = WanderSpeed;
		wanderAway.wanderAcceleration = WanderAcceleration;
		wanderAway.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
				case "Target_Reached":
				case "Collided_With_Environment":
					TargetRenderer.material.color = Color.cyan;
					GotoState((int)AIStates.Wander);
					break;
			}
		};
		
		attack.spawnObject = AttackSpawnObject;
		attack.attackTimeInSeconds = AttackTimeInSeconds;
		attack.spawnObjectCount = AttackSpawnObjectCount;
		attack.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Stun);
					break;
			}
		};

		meleeAttack.attackTimeInSeconds = 2.0f;
		meleeAttack.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					TargetRenderer.material.color = Color.green;
					GotoState((int)AIStates.Stun);
					break;
			}
		};

		stun.idleTimeInSeconds = StunTimeInSeconds;
		stun.OnMessage += (sender, args) =>
		{
			switch (args.Message)
			{
				case "Done":
					Anim.SetTrigger("Wander");
					TargetRenderer.material.color = Color.blue;
					GotoState((int)AIStates.WanderAway);
					break;
			}
		};
	}
	
	public void SpawnExplosion()
	{
		GameObject scorch =
			(GameObject)
				Instantiate(DamagePrefab,
				            new Vector3(this.transform.position.x,
				            UnityEngine.Random.Range(0.0001f, 0.2f),
				            this.transform.position.z),
				            Quaternion.Euler(270, UnityEngine.Random.Range(0, 360), 0));
		
		// Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.
		var damageSender = scorch.GetComponentInChildren<C3DamageSender>();
		damageSender.SendingLayer = Self.gameObject.layer;
		
		float smash_size = 4;
		scorch.transform.localScale = new Vector3(smash_size, smash_size, smash_size);
	}

    protected IEnumerator DoMeleeAttack(Transform from)
    {
        //        Debug.Log("Melee attack!");
        Anim.SetTrigger("Attack");

        yield return new WaitForSeconds(0.25f);

        // Instantiate the attack

        // Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.

        var atk = Instantiate(meleePrefab,
                              from.position,
                              from.rotation) as GameObject;

        // Set the sender of the damage so that it will affect the correct other parties
        var dmg = atk.GetComponentInChildren<C3DamageSender>();
        dmg.SendingLayer = (int)C3.Layers.Enemies;
        dmg.DamageAmount = 10;

        float smash_size = 2;
        atk.transform.localScale = new Vector3(smash_size, smash_size, smash_size);

        yield return null;
    }
	
	// Update is called once per frame
	public virtual void Update()
	{
		
		// Switch to locals so that the reset in base update doesn't lose these.
		bool isCollidingWithAttack = _isCollidingWithAttack;
		bool isCollidingWithPlayer = _isCollidingWithPlayer;
		bool isCollidingWithEnvironment = _isCollidingWithEnvironment;
		bool isCollidingWithEnemy = _isCollidingWithEnemy;
		base.Update();
		
		switch ( (AIStates)CurrentStateIndex )
		{
		case AIStates.Wander:
		{
			if (isCollidingWithAttack)
			{
				States[CurrentStateIndex].PostMessage("Collided_With_Attack");
				break;
			}
			else if (isCollidingWithPlayer)
			{
				States[CurrentStateIndex].PostMessage("Collided_With_Player");
				break;
			}
			else if ((Target.transform.position - Self.transform.position).magnitude < AttackDistance)
			{
				States[CurrentStateIndex].PostMessage("Player_In_Attack_Range");
				break;
			}
			else if (playerSighted())
			{
				States[CurrentStateIndex].PostMessage("Player_Sighted");
				break;
			}
			else if (isCollidingWithEnvironment)
			{
				States[CurrentStateIndex].PostMessage("Collided_With_Environment");
				break;
			}
			break;
		}
		case AIStates.WanderAway:
		{
			if (isCollidingWithEnvironment)
			{
				States[CurrentStateIndex].PostMessage("Collided_With_Environment");
				break;
			}
			break;
		}
		}
	}
	
}