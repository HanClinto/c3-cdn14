﻿using UnityEngine;
using System.Collections;

public class DestroyEveryThingThatTouchesMe : MonoBehaviour 
{
	void OnCollisionEnter(Collision collision)
	{
		Destroy (collision.gameObject);
	}
}
