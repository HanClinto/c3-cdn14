﻿using UnityEngine;
using System.Collections;

// Use this on a guiText or guiTexture object to automatically have them
// adjust their aspect ratio when the game starts.

public class GuiRatioFixer : MonoBehaviour
{
    public const float m_NativeRatio = 16.0f / 9.0f;

    void Start()
    {
        float currentRatio = (float)Screen.width / (float)Screen.height;
        Vector3 scale = transform.localScale;
        //scale.x *= m_NativeRatio / currentRatio;  // Fix the X ratio to match the Y
        scale.y *= currentRatio / m_NativeRatio; // Fix the Y ratio to match the X
        transform.localScale = scale;
    }
}