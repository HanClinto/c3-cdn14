﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class C3DamageSender : MonoBehaviour
{
    public float DamageAmount = 10.0f;
    public float KnockbackAmount = 10.0f;

    public int SendingLayer;

    public float Lifetime = 0.25f; // How long the damage collider stays around, for damage "persistence"

    protected float expireTime = 0;

    public void Awake()
    {
        expireTime = Time.time + Lifetime;
    }

    protected List<GameObject> alreadyHitList = new List<GameObject>();

    // OnTriggerEnter fires when this collides another mesh.  
    void OnTriggerStay(Collider other)
    {
        Debug.Log(String.Format("Trigger entered for object {0} on layer {1} vs. sending layer {2}", other.gameObject.name, other.gameObject.layer, SendingLayer));

        if (other.gameObject.layer != SendingLayer)
        {
            Debug.Log("Hit!");
            if (!alreadyHitList.Contains(other.gameObject))
            {
                Debug.Log("Hadn't already hit!");
//                Debug.Log(String.Format("2"));
//                Debug.Log(String.Format("Trigger entered for object {0} on layer {1} vs. sending layer {2}", other.gameObject.name, other.gameObject.layer, SendingLayer.value));
                alreadyHitList.Add(other.gameObject);

                var receivers = other.gameObject.GetComponents<C3DamageReceiver>();

                foreach (var receiver in receivers)
                {
//                    Debug.Log(String.Format("3"));

                    receiver.TakeDamage(
                        this, 
                        DamageAmount, 
                        this.transform.position - other.transform.position,
                        KnockbackAmount);
                }
            }
        }
    }

    void Update()
    {
        if (Time.time > expireTime)
        {
            this.enabled = false;
            Destroy(this.gameObject);
        }
    }
}