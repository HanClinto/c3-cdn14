// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class ReceiverItem {
	public GameObject receiver;
	public string action = "OnSignal";
	public float delay;
	
	public IEnumerator SendWithDelay ( MonoBehaviour sender  ){
		yield return new WaitForSeconds (delay);
		if (receiver)
			receiver.SendMessage (action);
		else
			Debug.LogWarning ("No receiver of signal \""+action+"\" on object "+sender.name+" ("+sender.GetType().Name+")", sender);
	}
}

public class SignalSender {
	public bool  onlyOnce;
	public ReceiverItem[] receivers;
	
	private bool  hasFired = false;
	
	public void  SendSignals ( MonoBehaviour sender  ){
		if (hasFired == false || onlyOnce == false) {
			for (int i= 0; i < receivers.Length; i++) {
				sender.StartCoroutine (receivers[i].SendWithDelay(sender));
			}
			hasFired = true;
		}
	}
}
