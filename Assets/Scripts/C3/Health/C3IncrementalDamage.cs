﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C3IncrementalDamage : MonoBehaviour
{
    public Mesh[] Meshes;
    
    public GameObject Self;
    public ParticleSystem DamageParticleEffect;
    public ParticleSystem DeathParticleEffect;

    private C3DamageReceiver Health;

    private int ActiveMeshIndex
    {
        get
        {
            float pct = Mathf.Clamp01(1 - (Health.Health / Health.MaxHealth));

            if (Health != null)
            {
                return Mathf.Clamp(
                    (int) (pct * Meshes.Length) - 1,
                    0,
                    Meshes.Length - 1);
            }

            return 0;
        }
    }

    private Mesh MeshToUse
    {
        get
        {
            if (Meshes.Length > 0)
                return Meshes[ActiveMeshIndex];

            return null;
        }
    }

    protected void ActivateCurrentMesh()
    {
        MeshFilter filter = GetComponent<MeshFilter>();

//        Debug.Log(String.Format("Health is {0}/{1}.  Mesh index is {2}/{3}", Health.Health, Health.MaxHealth, ActiveMeshIndex, Meshes.Length));

        filter.mesh = MeshToUse;

    }

    public Renderer TargetRenderer
    {
        get { return Self.GetComponentInChildren<Renderer>(); }
    }

    public virtual void Awake()
    {
        // Subscribe to our damage receiver
		if (Self == null) { Self = gameObject; }
        var r = Self.gameObject.GetComponent<C3DamageReceiver>();

        Health = r;

        Health.BeforeDamage += OnBeforeDamage;
        Health.AfterDamage += OnAfterDamage;
        Health.Died += OnDied;
    }

	public void Start()
	{
		ActivateCurrentMesh();
	}

    protected virtual void OnBeforeDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
        Debug.Log("Wall is threatening to take damage...");
        /*
        float atten = UnityEngine.Random.Range(0f, 1f);

        Debug.Log(String.Format("Entity {0} is due to receive {1} damage.  Attenuating by {2}.", Self.name, e.Amount, atten));

        e.DamageAttenuation = atten;
         */
    }

    protected virtual void OnAfterDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
//        Debug.Log(String.Format("Entity {0} has received {1} damage!", Self.name, e.Amount));
        Debug.Log("Wall is taking damage...");

        ActivateCurrentMesh();

        if (DamageParticleEffect != null)
        {
            DamageParticleEffect.Play();
        }
    }

    protected virtual void OnDied(object sender, C3DamageReceiver.DamageEventArgs e)
    {
        Debug.Log(String.Format("Entity {0} has died!", Self.name));

        StartCoroutine(DeathRoutine());
    }

    IEnumerator DeathRoutine()
    {
        if (DeathParticleEffect != null)
        {
            DeathParticleEffect.Play();
        }

//        yield return new WaitForSeconds(12);

        Destroy(Self);

        yield return null;
    }

    protected virtual void Update()
    {

    }

}

