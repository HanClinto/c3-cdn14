﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using System;
using UnityEngine;
using System.Collections;

public class C3DamageReceiver : MonoBehaviour
{
    public float MaxHealth = 100.0f;
    public float _Health;
    public float Health
    {
        get { return _Health; }
        set { _Health = value; }
    }

    public bool Dead
    {
        get { return Health <= 0; }
    }

    private float lastDamageTime = 0;

    public void Awake()
    {
        Health = MaxHealth;
    }

    public class DamageEventArgs : EventArgs
    {
        public C3DamageSender DamageSender;
        public C3DamageReceiver DamageReceiver;
        public float Amount;
        public Vector3 FromDirection;
        public float Knockback;
        
        // A way for the sender to attenuate / cancel the damage.  Increase to a maximum of 1 to attenuate all damage.
        public float DamageAttenuation = 0.0f;
    }

    public event EventHandler<DamageEventArgs> BeforeDamage;
    public event EventHandler<DamageEventArgs> AfterDamage;

    public event EventHandler<DamageEventArgs> Died;

    /// <summary>
    /// Send damage to the object
    /// </summary>
    /// <param name="amount">The amount of damage</param>
    /// <param name="fromDirection">The vector facing from this object to the sender</param>
    /// <param name="knockback">The amount of knockback to accompany this damage.</param>
    public void TakeDamage(C3DamageSender sender, float amount, Vector3 fromDirection, float knockback = 0)
    {
//        Debug.Log(String.Format("Attempting to take damage.  Current health is {0} of max health {1}", this.Health, this.MaxHealth));
        if (Dead)
            return;

        var e = new DamageEventArgs()
        {
            Amount = amount,
            DamageReceiver = this,
            DamageSender = sender,
            FromDirection = fromDirection,
            Knockback = knockback,
            DamageAttenuation = 0.0f
        };

        if (BeforeDamage != null)
            BeforeDamage(this, e);

        var finalAmount = amount * (1 - e.DamageAttenuation);

        // Take no damage if invincible, dead, or if the damage is zero
//        if (invincible)
//            return;
//        Debug.Log(string.Format("Dealing {0} damage.  Attenuation received back is {1}.  Final damage is {2}", e.Amount, e.DamageAttenuation, finalAmount));

        if (finalAmount <= 0)
            return;

        // Decrease health by damage and send damage signals
        Health -= finalAmount;
        e.Amount = finalAmount;

        if (AfterDamage != null)
            AfterDamage(this, e);

        lastDamageTime = Time.time;

        // Die if no health left
        if (Health <= 0)
        {
            if (Died != null)
                Died(this, e);
        }
    }
}