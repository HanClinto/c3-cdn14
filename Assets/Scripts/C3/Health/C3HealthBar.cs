﻿using UnityEngine;
using System;
using System.Collections;

public class C3HealthBar : MonoBehaviour {

	public float HealthBarXOffset = -10.0f;
	public float HealthBarYOffset = 50.0f;
	public float HealthBarHeight = 14.0f;
	public bool AttachToObjectPosition = true;
	public string OverlayText = "";

	private float _healthBarWidth = 0.0f;
	private string _healthBarText = "";
	private C3DamageReceiver _targetHealth = null;

	// Use this for initialization
	void Start () {
	
		_targetHealth = gameObject.GetComponent<C3DamageReceiver>();
		if (_targetHealth == null)
		{
			_healthBarWidth = 50;
		} 
		else 
		{
			_healthBarWidth = (float)(_targetHealth.MaxHealth * 1.85333f + 4.66667);
		}
	}
	
	// Update is called once per frame
	void OnGUI () {
		Rect rect = new Rect(0,0,0,0);
		if (AttachToObjectPosition)
		{
			Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);// gets screen position.
			screenPosition.y = Screen.height - (screenPosition.y + 1);// inverts y
			rect = new Rect(
				screenPosition.x - (_healthBarWidth / 2) + HealthBarXOffset, 
				screenPosition.y - HealthBarYOffset, 
				_healthBarWidth, HealthBarHeight
			);
		}
		else
		{
			rect = new Rect(
				HealthBarXOffset, 
				HealthBarYOffset, 
				_healthBarWidth, HealthBarHeight
				);
		}

		if (_targetHealth == null)
		{
			_healthBarText = "null ref";
		}
		else
		{
		    int cnt = Mathf.CeilToInt(_targetHealth.Health/5);
		    if (cnt < 0)
		        _healthBarText = "";
            else
                _healthBarText = new String('█', cnt);
		}

		GUI.color = new Color( 2 - (2 * _targetHealth.Health / _targetHealth.MaxHealth), 2 * _targetHealth.Health / _targetHealth.MaxHealth, 0, 1);
		GUI.skin.box.alignment = TextAnchor.MiddleRight;
		GUI.skin.box.fontSize = 14;
        GUI.Box(rect, _healthBarText);
		if (!String.IsNullOrEmpty(OverlayText)) { GUI.Box(rect, OverlayText); }
	}
}
