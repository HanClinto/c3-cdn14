﻿using UnityEngine;
using System;
using System.Collections;

public class C3EnemyCount : MonoBehaviour {
	
	public float XOffset = 5.0f;
	public float YOffset = 32.0f;
	public float StatusBarHeight = 30.0f;
	public string OverlayText = "";

	private int _statusInitialCount = 0;
	private float _statusBarWidth = 0.0f;
	private string _statusBarText = "";

	// Use this for initialization
	void Start () {
	
		_statusInitialCount = Math.Max(1, GameObject.FindGameObjectsWithTag("Enemy").Length);
		_statusBarWidth = (float)(_statusInitialCount * 17 + 7);
	}

    private bool triggeredEnding = false;
	
	// Update is called once per frame
	void OnGUI () {

		int currentStatusCount = GameObject.FindGameObjectsWithTag("Enemy").Length;

		if (currentStatusCount > _statusInitialCount)
		{
			_statusInitialCount = currentStatusCount;
			_statusBarWidth = (float)(_statusInitialCount * 17 + 7);
		}

		Rect rect = new Rect(
			XOffset, 
			YOffset, 
			_statusBarWidth, StatusBarHeight
		);

		_statusBarText = new String('☣', currentStatusCount);

		GUI.color = new Color(2 * currentStatusCount / _statusInitialCount, 2 - (2 * currentStatusCount / _statusInitialCount), 0, 1);
		GUI.skin.textField.alignment = TextAnchor.MiddleRight;
		GUI.skin.textField.fontSize = 22;
		GUI.TextField(rect, _statusBarText);
		if (!String.IsNullOrEmpty(OverlayText)) { GUI.TextField(rect, OverlayText); }

//        Debug.Log(String.Format("Currently have {0} enemies", currentStatusCount));

	    if (currentStatusCount == 0)
	    {
            alpha -= fadeDir * fadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);

	        GUI.color = new Color(0, 0, 0, alpha);

            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), 
                            fadeTexture);

//            Debug.Log(String.Format("Alpha is {0}", alpha));
	        if (alpha >= 1)
	        {
	            Application.LoadLevel(Application.loadedLevel + 1);
	        }
	    }
	}

    public Texture2D fadeTexture;
    private float fadeSpeed = 0.2f;
    private int drawDepth = -1000;
    private float alpha = 0.0f;
    private int fadeDir = -1;

}
