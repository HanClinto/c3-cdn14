// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class PlayerMoveController : MonoBehaviour {
    public Animator Anim;
	
	
	// Objects to drag in
	public MovementMotor motor;
	public Transform character;
	public GameObject cursorPrefab;
	public GameObject joystickPrefab;

    public GameObject orbiter;

    // Combat configuration
    public GameObject meleePrefab;
    public Transform meleeTransform;

    public GameObject rangedPrefab;

	// Settings
	public float cameraSmoothing = 0.01f;
	public float cameraPreview = 2.0f;

    
    public float meleeCooldown = 0.75f;
    private float lastMeleeTime;

    public int Rage = 1;
    public int MaxRage = 10;

	// Cursor settings
	public float cursorPlaneHeight = 0;
	public float cursorFacingCamera = 0;
	public float cursorSmallerWithDistance = 0;
	public float cursorSmallerWhenClose = 1;
	
	// Private memeber data
	private Camera mainCamera;
	
	private Transform cursorObject;
	private Joystick joystickLeft;
	private Joystick joystickRight;
	
	private Transform mainCameraTransform;
	private Vector3 cameraVelocity = Vector3.zero;
	private Vector3 cameraOffset = Vector3.zero;
	private Vector3 initOffsetToPlayer;
	
	// Prepare a cursor point varibale. This is the mouse position on PC and controlled by the thumbstick on mobiles.
	private Vector3 cursorScreenPosition;
	
	private Plane playerMovementPlane;
	
	private GameObject joystickRightGO;
	
	private Quaternion screenMovementSpace;
	private Vector3 screenMovementForward;
	private Vector3 screenMovementRight;
	
	public void  Awake (){		
		motor.movementDirection = Vector2.zero;
		motor.facingDirection = Vector2.zero;
		
		// Set main camera
		mainCamera = Camera.main;
		mainCameraTransform = mainCamera.transform;
		
		// Ensure we have character set
		// Default to using the transform this component is on
		if (!character)
			character = transform;
		
		initOffsetToPlayer = mainCameraTransform.position - character.position;
		
		#if !UNITY_FLASH
		if (cursorPrefab) {
			cursorObject = (Instantiate (cursorPrefab) as GameObject).transform;
		}
		#endif
		
		// Save camera offset so we can use it in the first frame
		cameraOffset = mainCameraTransform.position - character.position;
		
		// Set the initial cursor position to the center of the screen
		cursorScreenPosition = new Vector3 (0.5f * Screen.width, 0.5f * Screen.height, 0);
		
		// caching movement plane
		playerMovementPlane = new Plane (character.up, character.position + character.up * cursorPlaneHeight);
	}
	
	public void  Start (){
		// it's fine to calculate this on Start () as the camera is static in rotation
		
		screenMovementSpace = Quaternion.Euler (0, mainCameraTransform.eulerAngles.y, 0);
		screenMovementForward = screenMovementSpace * Vector3.forward;
		screenMovementRight = screenMovementSpace * Vector3.right;

        // Subscribe to our damage receiver
        var rs = gameObject.GetComponents<C3DamageReceiver>();

        foreach (var r in rs)
        {
            r.BeforeDamage += OnBeforeDamage;
            r.AfterDamage += OnAfterDamage;
            r.Died += OnDied;
        }

//        fadeTexture = new Texture2D(512, 512);
	}

    protected virtual void OnBeforeDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
    }

    protected virtual void OnAfterDamage(object sender, C3DamageReceiver.DamageEventArgs e)
    {
    }

    protected virtual void OnDied(object sender, C3DamageReceiver.DamageEventArgs e)
    {
//        dying = true;
//        dyingTime = Time.unscaledTime;

        Time.timeScale = 0.1f;

        Invoke("ReloadLevel", .5f);

        iTween.CameraFadeAdd();
        iTween.CameraFadeDepth(0);
        iTween.CameraFadeTo(iTween.Hash("amount", 1, "time", 1));
//        iTween.CameraFadeTo(iTween.Hash("amount", 1, "time", 1, "oncomplete", "ReloadScene", "oncompletetarget", gameObject)); 
//        iTween.CameraFadeTo(0, .5f);
    }

    protected void ReloadLevel()
    {
        Time.timeScale = 1f;
        Application.LoadLevel(Application.loadedLevel);        
    }

    /*
    private bool dying = false;
    private float dyingTime = 0;
    public Texture2D fadeTexture; 
    private float fadeSpeed = 0.2f;
    private int drawDepth = -1000;
    private float alpha = 0.0f;
    private int fadeDir = -1;

    protected void OnGui()
    {
        if (dying)
        {
            alpha = 1; // fadeDir*fadeSpeed*(Time.unscaledTime - dyingTime);
            alpha = Mathf.Clamp01(alpha);

            GUI.color = new Color(127, 0, 0, alpha);

            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height),
                            fadeTexture);
        }
    }
     * */
	
	public void  OnDisable (){
		if (joystickLeft) 
			joystickLeft.enabled = false;
		
		if (joystickRight)
			joystickRight.enabled = false;
	}
	
	public void  OnEnable (){
		if (joystickLeft) 
			joystickLeft.enabled = true;
		
		if (joystickRight)
			joystickRight.enabled = true;
	}
	
	public void  Update (){
		// HANDLE CHARACTER MOVEMENT DIRECTION
        motor.movementDirection = Input.GetAxis("Horizontal") * screenMovementRight + Input.GetAxis("Vertical") * screenMovementForward;
		
		// Make sure the direction vector doesn't exceed a length of 1
		// so the character can't move faster diagonally than horizontally or vertically
		if (motor.movementDirection.sqrMagnitude > 1)
			motor.movementDirection.Normalize();
		
		
		// HANDLE CHARACTER FACING DIRECTION AND SCREEN FOCUS POINT
		
		// First update the camera position to take into account how much the character moved since last frame
		//mainCameraTransform.position = Vector3.Lerp (mainCameraTransform.position, character.position + cameraOffset, Time.deltaTime * 45.0ff * deathSmoothoutMultiplier);
		
		// Set up the movement plane of the character, so screenpositions
		// can be converted into world positions on this plane
		//playerMovementPlane = new Plane (Vector3.up, character.position + character.up * cursorPlaneHeight);
		
		// optimization (instead of newing Plane):
		
		playerMovementPlane.normal = character.up;
		playerMovementPlane.distance = -character.position.y + cursorPlaneHeight;
		
		// used to adjust the camera based on cursor or joystick position
		
		Vector3 cameraAdjustmentVector = Vector3.zero;
		
		// On PC, the cursor point is the mouse position
		Vector3 cursorScreenPosition = Input.mousePosition;
		
		// Find out where the mouse ray intersects with the movement plane of the player
		Vector3 cursorWorldPosition = ScreenPointToWorldPointOnPlane (cursorScreenPosition, playerMovementPlane, mainCamera);
		
		float halfWidth = Screen.width / 2.0f;
		float halfHeight = Screen.height / 2.0f;
		float maxHalf = Mathf.Max (halfWidth, halfHeight);
		
		// Acquire the relative screen position			
		Vector3 posRel = cursorScreenPosition - new Vector3 (halfWidth, halfHeight, cursorScreenPosition.z);		
		posRel.x /= maxHalf; 
		posRel.y /= maxHalf;
		
		cameraAdjustmentVector = posRel.x * screenMovementRight + posRel.y * screenMovementForward;
		cameraAdjustmentVector.y = 0.0f;	
		
		// The facing direction is the direction from the character to the cursor world position
		motor.facingDirection = (cursorWorldPosition - character.position);
		motor.facingDirection.y = 0;			
		
		// Draw the cursor nicely
		HandleCursorAlignment (cursorWorldPosition);

		
        // HANDLE ORBITER ROTATION

        Vector3 orbiterLookPosition = new Vector3(cursorWorldPosition.x, 
                                                  orbiter.transform.position.y, 
                                                  cursorWorldPosition.z);
	    Vector3 orbiterLookVector = orbiterLookPosition - orbiter.transform.position;

	    if (!orbiterLookVector.Equals(Vector3.zero))
	    {
	        Quaternion orbiterDest = Quaternion.LookRotation(orbiterLookPosition - orbiter.transform.position);

	        const float lerpStrength = 5f;

	        orbiter.transform.rotation = Quaternion.Lerp(orbiter.transform.rotation, orbiterDest,
	            Mathf.Min(1.0f, Time.deltaTime*lerpStrength));
	    }

	    // HANDLE CAMERA POSITION
		
		// Set the target position of the camera to point at the focus point
		Vector3 cameraTargetPosition = character.position + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		
		// Apply some smoothing to the camera movement
		mainCameraTransform.position = Vector3.SmoothDamp (mainCameraTransform.position, cameraTargetPosition, ref cameraVelocity, cameraSmoothing);
		
		// Save camera offset so we can use it in the next frame
		cameraOffset = mainCameraTransform.position - character.position;

        
        // HANDLE MELEE ATTACKS
	    if (Input.GetMouseButton(0))
	    {
	        var nextAttack = lastMeleeTime + meleeCooldown;

	        if (Time.time > nextAttack)
	        {
	            lastMeleeTime = Time.time;
                
                // Spawn a melee attack
	            StartCoroutine(DoMeleeAttack(character));
	        }
	    }

        // HANDLE RANGED ATTACKS
	    if (Input.GetMouseButtonDown(1))
	    {
	        StartCoroutine(DoRangedAttack(orbiter.transform));
	    }

        // HANDLE DASH
	    if (Input.GetKeyDown(KeyCode.Space))
	    {
	        StartCoroutine(DoDash());
	    }

	    if (Anim != null)
	    {
//            Debug.Log(
//                string.Format("Motor speed is {0}.  Direction is {1}.  Transform velocity is {2}",
//                    motor.movementSpeed,
//                    motor.movementDirection.magnitude,
//                    this.rigidbody.velocity.magnitude));

            Anim.SetFloat("Speed", rigidbody.velocity.magnitude);
	    }
	}

    protected IEnumerator DoMeleeAttack(Transform from)
    {
//        Debug.Log("Melee attack!");
        Rage = Mathf.Min(Rage + 1, MaxRage);

        Anim.SetTrigger("MeleeAttack");

        yield return new WaitForSeconds(0.25f);

        // Instantiate the attack

        // Set the layer of the damage sender to our own layer so that we (and our allies) don't get damaged by it.

        var atk = Instantiate(meleePrefab, 
                              meleeTransform.position, 
                              meleeTransform.rotation) as GameObject;

        // Set the sender of the damage so that it will affect the correct other parties
        var dmg = atk.GetComponentInChildren<C3DamageSender>();
//        dmg.SendingLayer = (int)C3.Layers.Player;
        dmg.DamageAmount = 10;

//        float smash_size = 4;
//        atk.transform.localScale = new Vector3(smash_size, smash_size, smash_size);

        yield return null;
    }

    protected IEnumerator DoRangedAttack(Transform from)
    {
        float pct = Mathf.Min( ((float)Rage) / MaxRage, 1.0f);
        //Rage = 1;
        // Decrease Rage to a minimum of 1
        Rage = Mathf.Max(Rage - 1, 1);

        const float maxMeleeMissileVelocity = 30f;

//        Debug.Log(string.Format("Doing ranged attack at {0}%", (int)(pct * 100)));

        // HACK: Set PCT to 1 for now to fire at full strength
        //pct = 1;

        var atk = Instantiate(rangedPrefab,
                              orbiter.transform.position,
                              Quaternion.identity) as GameObject;


        // Set the sender of the damage so that it will affect the correct other parties
        var dmg = atk.GetComponentInChildren<C3DamageSender>();
        dmg.SendingLayer = (int)C3.Layers.Player;
        dmg.DamageAmount = 10 * pct;

        atk.transform.localScale = new Vector3(pct, pct, pct);
        // TODO: Move the attack towards the cursor
        //atk.rigidbody.velocity = 

        atk.rigidbody.velocity = from.forward*maxMeleeMissileVelocity; //* (.5f * pct);

//        atk.rigidbody.velocity = new Vector3(1, 0, 1);

        yield return null;
    }

    protected IEnumerator DoDash()
    {
        Rage = Mathf.Min(Rage + 1, MaxRage);

        Anim.SetTrigger("Teleport");

        yield return null;
    }
	
	public static Vector3 PlaneRayIntersection ( Plane plane , Ray ray  ){
		float dist;
		plane.Raycast (ray, out dist);
		return ray.GetPoint (dist);
	}
	
	public static Vector3 ScreenPointToWorldPointOnPlane ( Vector3 screenPoint ,   Plane plane ,   Camera camera  ){
		// Set up a ray corresponding to the screen position
		Ray ray = camera.ScreenPointToRay (screenPoint);
		
		// Find out where the ray intersects with the plane
		return PlaneRayIntersection (plane, ray);
	}
	
	void  HandleCursorAlignment ( Vector3 cursorWorldPosition  ){
		if (!cursorObject)
			return;
		
		// HANDLE CURSOR POSITION
		
		// Set the position of the cursor object
		cursorObject.position = cursorWorldPosition;
		
		#if !UNITY_FLASH
		// Hide mouse cursor when within screen area, since we're showing game cursor instead
		Screen.showCursor = (Input.mousePosition.x < 0 || Input.mousePosition.x > Screen.width || Input.mousePosition.y < 0 || Input.mousePosition.y > Screen.height);
		#endif
		
		// HANDLE CURSOR ROTATION
		
		Quaternion cursorWorldRotation = cursorObject.rotation;
		if (motor.facingDirection != Vector3.zero)
			cursorWorldRotation = Quaternion.LookRotation (motor.facingDirection);
		
		// Calculate cursor billboard rotation
		Vector3 cursorScreenspaceDirection = Input.mousePosition - mainCamera.WorldToScreenPoint (transform.position + character.up * cursorPlaneHeight);
		cursorScreenspaceDirection.z = 0;
		Quaternion cursorBillboardRotation = mainCameraTransform.rotation * Quaternion.LookRotation (cursorScreenspaceDirection, -Vector3.forward);
		
		// Set cursor rotation
		cursorObject.rotation = Quaternion.Slerp (cursorWorldRotation, cursorBillboardRotation, cursorFacingCamera);
		
		
		// HANDLE CURSOR SCALING
		
		// The cursor is placed in the world so it gets smaller with perspective.
		// Scale it by the inverse of the distance to the camera plane to compensate for that.
		float compensatedScale = 0.1f * Vector3.Dot (cursorWorldPosition - mainCameraTransform.position, mainCameraTransform.forward);
		
		// Make the cursor smaller when close to character
		float cursorScaleMultiplier = Mathf.Lerp (0.7f, 1.0f, Mathf.InverseLerp (0.5f, 4.0f, motor.facingDirection.magnitude));
		
		// Set the scale of the cursor
		cursorObject.localScale = Vector3.one * Mathf.Lerp (compensatedScale, 1, cursorSmallerWithDistance) * cursorScaleMultiplier;
		
		// DEBUG - REMOVE LATER
		if (Input.GetKey(KeyCode.O)) cursorFacingCamera += Time.deltaTime * 0.5f;
		if (Input.GetKey(KeyCode.P)) cursorFacingCamera -= Time.deltaTime * 0.5f;
		cursorFacingCamera = Mathf.Clamp01(cursorFacingCamera);
		
		if (Input.GetKey(KeyCode.K)) cursorSmallerWithDistance += Time.deltaTime * 0.5f;
		if (Input.GetKey(KeyCode.L)) cursorSmallerWithDistance -= Time.deltaTime * 0.5f;
		cursorSmallerWithDistance = Mathf.Clamp01(cursorSmallerWithDistance);
	}
}