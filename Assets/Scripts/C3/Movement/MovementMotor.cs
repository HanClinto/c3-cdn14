using UnityEngine;
using System.Collections;

/// <summary>
/// This class can be used like an interface.
/// Inherit from it to define your own movement motor that can control
///  the movement of characters, enemies, or other entities.
/// </summary>
public class MovementMotor : MonoBehaviour {
	// The direction the character wants to move in, in world space.
	// The vector should have a length between 0 and 1.
	//	@HideInInspector
	public Vector3 movementDirection;
	public float movementSpeed = 5.0f;
	public float movementAcceleration = 1.0f;

	// Simpler motors might want to drive movement based on a target purely
	//	@HideInInspector
	public Vector3 movementTarget;
	
	// The direction the character wants to face towards, in world space.
	//	@HideInInspector
	public Vector3 facingDirection;
	
}