﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

//@script RequireComponent (Rigidbody)

public class HoverMovementMotor : MovementMotor {
	
	//public MoveController movement;
	public float turningSpeed = 3.0f;
	public float turningSnappyness = 3.0f;
	public float bankingAmount = 1.0f;
	
	public void FixedUpdate (){
		// Handle the movement of the character
		Vector3 targetVelocity = movementDirection * movementSpeed;
		Vector3 deltaVelocity = targetVelocity - rigidbody.velocity;
		rigidbody.AddForce (deltaVelocity * movementAcceleration, ForceMode.Acceleration);
		
		// Make the character rotate towards the target rotation
		//Vector3 facingDir = facingDirection != Vector3.zero ? facingDirection : movementDirection;
		Vector3 facingDir = movementTarget - transform.position;
		if (facingDir != Vector3.zero) {
			var targetRotation= Quaternion.LookRotation (facingDir, Vector3.up);
			var deltaRotation= targetRotation * Quaternion.Inverse(transform.rotation);
			Vector3 axis;
			float angle;
			deltaRotation.ToAngleAxis (out angle, out axis);
			Vector3 deltaAngularVelocity = axis * Mathf.Clamp (angle, -turningSpeed, turningSpeed) - rigidbody.angularVelocity;
			
			float banking = Vector3.Dot (movementDirection, -transform.right);
			
			rigidbody.AddTorque (deltaAngularVelocity * turningSnappyness + movementDirection * banking * bankingAmount);
		}
	}
	
	public void OnCollisionStay ( Collision collisionInfo  ){
		// Move up if colliding with static geometry
		if (collisionInfo.rigidbody == null)
			rigidbody.velocity += Vector3.up * Time.deltaTime * 50;
	}
}
