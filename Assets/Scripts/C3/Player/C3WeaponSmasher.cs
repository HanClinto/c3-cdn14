﻿using UnityEngine;
using System.Collections;

public class C3WeaponSmasher : MonoBehaviour {

	public GameObject SmashDecal;

	public bool isSmashing = false;

	public float smashSpeed = 20;
	public float maxChargeTime = 3.0f;

	public float chargeStart;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// If we're not smashing, then check to see if we should smash.
		if (!isSmashing) 
		{
			// If we've just pressed the mouse button
			if (Input.GetMouseButtonDown (0)) {
				// Then keep track of the time that we started holding down the mouse button
				chargeStart = Time.time;
			}
			else if (Input.GetMouseButtonUp (0)){
				// If we've just let go of the mouse button, then figure out how long we were holding the button down
				float delta = Time.time - chargeStart;

				Debug.Log (string.Format ("Smashing with delta of {0}, speed is {1}!", delta, smashSpeed * (delta / maxChargeTime)));

				// TODO: Check to make sure we're smashing with enough velocity at some threshold?

				isSmashing = true;
				this.rigidbody.constraints = RigidbodyConstraints.None;

				// Figure out the speed based on our mouse-hold delta
				float speed = Mathf.Min ( smashSpeed, smashSpeed * (delta / maxChargeTime) );

				float momentum = this.rigidbody.velocity.magnitude;

				// Shoot the body straight upwards so that it swings over our head and into the ground on the other side.
				this.rigidbody.velocity = new Vector3(0, speed + momentum, 0);
			}
		} else {

		}
	}

	// OnTriggerEnter fires when this ball collides another mesh.  In our instances, this will be when it's hitting either the ground or an enemy.
	// For right now, we're only dealing with the "Ground" case, where we leave a crater / scorch mark upon our impact.  
	// Later, we will want to spawn an explosive force here that knocks enemies back and damages them as well.
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Ground") {
			this.transform.position = new Vector3(this.transform.position.x,
			                                      1.0f,
			                                      this.transform.position.z);

			float collisionMagnitude = this.rigidbody.velocity.magnitude;

			this.rigidbody.velocity = Vector3.zero;
			this.rigidbody.constraints = RigidbodyConstraints.FreezePositionY;

			if (isSmashing)
			{
				Debug.Log ("SMASH!");

				GameObject scorch = 
					(GameObject) 
						Instantiate ( SmashDecal, 
				             new Vector3(this.transform.position.x,
						            	  Random.Range(0.0001f, 0.2f),
	                                      this.transform.position.z),
				             Quaternion.Euler (270, Random.Range(0, 360), 0));

				float smash_size = 10 * collisionMagnitude / smashSpeed;
				scorch.transform.localScale = new Vector3( smash_size, smash_size, smash_size );

				isSmashing = false;
			}
		}
	}
}
