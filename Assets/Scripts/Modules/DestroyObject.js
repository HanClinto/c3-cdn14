#pragma strict

var objectToDestroy : GameObject;

function DestroyObject () {
	if (objectToDestroy) {
		Spawner.Destroy (objectToDestroy);
	} else {
		Spawner.Destroy (gameObject);
	}
}

