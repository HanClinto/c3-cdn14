﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {
	public bool paused = false;
	// Use this for initialization
	void Start () {
		Time.timeScale = 1;

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("escape")){
			
			if (!paused){
				Pause();
			}
			else if (paused) {
				Unpause();
			}
		}
		Debug.Log(paused);
	
	}

	public void Pause(){
		Time.timeScale = 0;
		paused = true;
		//spawn a pause menu
	}
	public void Unpause(){
		Time.timeScale = 1;
		paused = false;
		//destroy pause menu, alternatively just have the menu destroy itself
		//whenever timescale=1 or paused=false
	}
}
