﻿using UnityEngine;
using System.Collections;

public class Introscene : MonoBehaviour {
	// This is to be used during the intro sequence letting the animation and sound play
	//and then loading the 
	public float delay = 1.0f;
	// Use this for initialization
	void Start () {
		StartCoroutine(Intro());
	}

	IEnumerator Intro(){
		yield return new WaitForSeconds(delay); //change this value to the length of the intro animaton
		Application.LoadLevel(1); //load first level or cutscene, change to level name or number
	}
}
