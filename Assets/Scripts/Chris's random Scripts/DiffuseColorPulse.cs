﻿using UnityEngine;
using System.Collections;

public class DiffuseColorPulse : MonoBehaviour {

	//This script is for pulsating between two colors on enemies and objects

	public Color lowColor;
	public Color highColor;
	public float pulseTime;
	
	// Update is called once per frame
	void Update () {
		if (gameObject.renderer.material.color == highColor) iTween.ColorTo(gameObject, lowColor, pulseTime);
		if (gameObject.renderer.material.color == lowColor) iTween.ColorTo(gameObject, highColor, pulseTime);
	}
}
