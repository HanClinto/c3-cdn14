﻿using UnityEngine;
using System.Collections;

public class DelayedDestroy : MonoBehaviour {
	public float delay = 1.0f;

	// This script is for destroying objects after a delay
	//useful for particles, enemies, bullets etc

	void Start () {
		Destroy(gameObject, delay);
	}
}
