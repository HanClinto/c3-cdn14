﻿using UnityEngine;
using System.Collections;

public class OrbiterGlow : MonoBehaviour {

	public Transform camera;
	public Vector3 highScale;
	public Vector3 lowScale;
	public float scaleTime;

	void Start () {
		if (camera == null) {
			camera = Camera.main.transform;
		}
		iTween.Defaults.easeType = iTween.EaseType.easeInOutQuad;
	}

	// Update is called once per frame
	void Update () {
		transform.LookAt (camera);
		if (transform.localScale == lowScale) {
			iTween.ScaleTo(gameObject, highScale, scaleTime);		
		}
		if (transform.localScale == highScale) {
			iTween.ScaleTo(gameObject, lowScale, scaleTime);
		}
	}
}
