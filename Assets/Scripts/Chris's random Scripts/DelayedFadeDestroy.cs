﻿using UnityEngine;
using System.Collections;

public class DelayedFadeDestroy : MonoBehaviour
{
    public float delay = 1.0f;

    public float fadeOutAt = .5f;

    // This script is for destroying objects after a delay
    //useful for particles, enemies, bullets etc

    void Start()
    {
        Destroy(gameObject, delay);

        Invoke("fadeOut", fadeOutAt);
    }

    void fadeOut()
    {
        iTween.FadeTo(gameObject, 0, delay - fadeOutAt);
    }


}
