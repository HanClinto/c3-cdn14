﻿// FadeInOut
//
//--------------------------------------------------------------------
//                        Public parameters
//--------------------------------------------------------------------
 
public var fadeOutTexture : Texture2D;
public var fadeSpeed = 0.3;
public var delay = 4.5;

 
var drawDepth = -1000;
 
//--------------------------------------------------------------------
//                       Private variables
//--------------------------------------------------------------------
 
private var alpha = 1.0; 
 
private var fadeDir = -1;
 
//--------------------------------------------------------------------
//                       Runtime functions
//--------------------------------------------------------------------
 
//--------------------------------------------------------------------
 
 function Start(){
	Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
 	//you can make it windowed by default using this code:
 	//Screen.SetResolution (1024, 768, false) //<enter desired resolution
	alpha=1;
	fadeIn();
}
 
/* 
 function Update(){
 	if (Input.anyKeyDown){
 		Application.LoadLevel(1);
 	}
 }
 */

function OnGUI(){
 
	alpha += fadeDir * fadeSpeed * Time.deltaTime;	
	alpha = Mathf.Clamp01(alpha);	
 
	GUI.color.a = alpha;
 
	GUI.depth = drawDepth;
 
	GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
}
 
//--------------------------------------------------------------------
 
function fadeIn(){
	fadeDir = -1;
	yield WaitForSeconds (delay);	
	fadeOut();
	yield WaitForSeconds (fadeSpeed+ 1.4);
	Application.Quit();
//	Application.LoadLevel(1);
}
 
//--------------------------------------------------------------------
 
function fadeOut(){
	fadeDir = 1;	
}
